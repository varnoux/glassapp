﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Media.Media3D;

namespace glassApp
{
    internal class Obj
    {
        public Obj() {
            Vertices = new List<Vector3D>();
            Normals = new List<Vector3D>();
            Faces = new List<KeyValuePair<double,double>[]>();
            Groupes_face = new List<int[]>();
            Groupes_edge = new List<int[]>();
        }

        public Obj(Obj obj)
        {
            //Vertices = new List<Vector3D>(obj.Vertices);
            Vertices = new List<Vector3D>();
            Normals = new List<Vector3D>(obj.Normals);
            Faces = new List<KeyValuePair<double, double>[]>(obj.Faces);
        }

        public List<Vector3D> Vertices { get; set; }
        public List<Vector3D> Normals { get; set; }
        public List<KeyValuePair<double, double>[]> Faces { get; set; }
        public List<int[]> Groupes_face { get; }
        public List<int[]> Groupes_edge { get; }

        public Vector3D VerticeNormal(int index)
        {
            Vector3D n = new Vector3D(0, 0, 0);
            double number_n = 0;

            foreach (KeyValuePair<double, double>[] pairs in Faces)
            {
                if (pairs[0].Key-1 == index)
                {
                    n = Vector3D.Add(n, Normals[(int)(pairs[0].Value-1)]);
                    number_n++;
                    continue;
                }
                if (pairs[1].Key-1 == index)
                {
                    n = Vector3D.Add(n, Normals[(int)(pairs[1].Value-1)]);
                    number_n++;
                    continue;
                }
                if (pairs[2].Key-1 == index)
                {
                    n = Vector3D.Add(n, Normals[(int)(pairs[2].Value-1)]);
                    number_n++;
                    continue;
                }
            }

            n = Vector3D.Divide(n, number_n);
            n.Normalize();

            return n;
        }

        public int isAlreadyCreate(KeyValuePair<double, double>[] face)
        {

            for(int numFace = 0; numFace < Faces.Count; numFace++)
            {
                KeyValuePair<double, double>[] face_cur = Faces[numFace];
                if (face_cur.Length != 4)
                    continue;

                List<double> l = new List<double>();
                l.Add(face_cur[0].Key);
                l.Add(face_cur[1].Key);
                l.Add(face_cur[2].Key);
                l.Add(face_cur[3].Key);

                if (!l.Contains(face[0].Key))
                    continue;

                if (!l.Contains(face[1].Key))
                    continue;

                if (!l.Contains(face[2].Key))
                    continue;

                if (!l.Contains(face[3].Key))
                    continue;

                return numFace;
            }

            return -1;
        }

        public void Bridge(Obj obj)
        {
            int nbFaces = obj.Faces.Count;
            int nbVertices = obj.Vertices.Count;
            int numfaces = Faces.Count - 1;
            int max = (Faces.Count - 1) - nbFaces;

            List<int> numFace = new List<int>();

            for (; numfaces > max; numfaces--)
            {
                KeyValuePair<double, double>[] pairs = Faces[numfaces];

                for(int numPairs = 0; numPairs < pairs.Length; numPairs++)
                {
                    int index_current_point = (int)(pairs[numPairs].Key);
                    int index_next_point = (int)(pairs[(numPairs+1)% pairs.Length].Key);
                    int index_point_for_normal = (int)(pairs[(numPairs + 2) % pairs.Length].Key);

                    KeyValuePair<double, double>[] n_pairs = new KeyValuePair<double, double>[4];
                    n_pairs[0] = new KeyValuePair<double, double>(index_current_point, Normals.Count);
                    n_pairs[1] = new KeyValuePair<double, double>(index_next_point, Normals.Count);
                    n_pairs[2] = new KeyValuePair<double, double>(index_next_point - nbVertices, Normals.Count);
                    n_pairs[3] = new KeyValuePair<double, double>(index_current_point - nbVertices, Normals.Count);

                    int indexFace = isAlreadyCreate(n_pairs);
                    if (indexFace < 0)
                    {
                        Console.WriteLine(Faces.Count);
                        numFace.Add(Faces.Count);

                        Vector3D p1 = Vertices[index_current_point-1];
                        Vector3D p2 = Vertices[index_next_point-1];
                        Vector3D pn = Vertices[index_point_for_normal-1];

                        Vector3D v = p2 - p1;
                        v.Normalize();

                        Vector3D v2= pn - p1;

                        Vector3D ph = p1 + v * Vector3D.DotProduct(v2, v);

                        Vector3D Normal = pn - ph;
                        Normal.Normalize();

                        Normals.Add(Normal);

                        Faces.Add(n_pairs);
                    }
                    else
                    {
                        numFace.Remove(indexFace);
                        int offset = (Faces.Count-1) - indexFace;
                        for (int i = numFace.Count - 1; i > (numFace.Count - 1) - offset; i--)
                            numFace[i]--;
                        Console.WriteLine(indexFace +" "+ Faces.Count);
                        Normals.RemoveAt((int)Faces[numfaces][0].Value);
                        Faces.RemoveAt(indexFace);
                    }
                }
            }

            Groupes_edge.Add(numFace.ToArray());
        }

        public void Translate(double thickness)
        {
            for(int i = 0; i < Vertices.Count; i++)
            {
                Vector3D v = Vertices[i];

                Vector3D n = VerticeNormal(i);

                Vertices[i] = Vector3D.Add(v, Vector3D.Multiply(thickness/10, -n));
            }
        }

        public void AddObj(Obj obj,bool invert)
        {
            int offset_vertices = 0;
            if(Vertices.Count > 0)
                offset_vertices = Vertices.Count;

            int offset_normals = 0;
            if (Normals.Count > 0)
                offset_normals = Normals.Count;

            Vertices.AddRange(obj.Vertices);
            Normals.AddRange(obj.Normals);

            List<int> numFace = new List<int>();

            foreach (KeyValuePair<double, double>[] pairs in obj.Faces){
                numFace.Add(Faces.Count);
                KeyValuePair<double, double>[] n_pairs = new KeyValuePair<double, double>[3];
                if (!invert)
                {
                    n_pairs[0] = new KeyValuePair<double, double>(pairs[0].Key + offset_vertices, pairs[0].Value + offset_normals);
                    n_pairs[1] = new KeyValuePair<double, double>(pairs[1].Key + offset_vertices, pairs[1].Value + offset_normals);
                }
                else
                {
                    n_pairs[1] = new KeyValuePair<double, double>(pairs[0].Key + offset_vertices, pairs[0].Value + offset_normals);
                    n_pairs[0] = new KeyValuePair<double, double>(pairs[1].Key + offset_vertices, pairs[1].Value + offset_normals);
                }
                n_pairs[2] = new KeyValuePair<double, double>(pairs[2].Key+offset_vertices,pairs[2].Value + offset_normals);
                Faces.Add(n_pairs);
            }
            Groupes_face.Add(numFace.ToArray());
        }

        public void LoadFromFile(string filename)
        {
            string[] Lines = System.IO.File.ReadAllLines(filename);

            foreach(string Line in Lines)
            {
                var format = new NumberFormatInfo();
                format.NegativeSign = "-";
                format.NumberDecimalSeparator = ".";

                RegexOptions options = RegexOptions.None;
                Regex regex = new Regex("[ ]{2,}", options);
                String new_line = regex.Replace(Line, " ");
                new_line = new_line.TrimEnd();

                string[] components = new_line.Split(' ');

                if (components[0].Equals("v"))
                {
                    Vertices.Add(new Vector3D(
                        Double.Parse(components[1], format),
                        Double.Parse(components[2], format),
                        Double.Parse(components[3], format)
                        ));
                }

                if (components[0].Equals("vn"))
                {
                    Normals.Add(new Vector3D(
                        Double.Parse(components[1], format),
                        Double.Parse(components[2], format),
                        Double.Parse(components[3], format)
                        ));
                }

                if (components[0].Equals("f"))
                {
                    KeyValuePair<double, double>[] pairs = null;
                    
                    for (int i = 2; i <= components.Length-2; i++)
                    {
                        pairs = new KeyValuePair<double, double>[3];
                        string[] face_components = components[1].Split('/');
                        if (face_components.Length == 3)
                        {
                            pairs[0] = new KeyValuePair<double, double>(
                                Convert.ToDouble(face_components[0]),
                                Convert.ToDouble(face_components[2])
                                );
                        }
                        else
                        {
                            if (face_components.Length == 2)
                            {
                                pairs[0] = new KeyValuePair<double, double>(
                                    Convert.ToDouble(face_components[0]),
                                    Convert.ToDouble(face_components[1])
                                    );
                            }
                        }

                        face_components = components[i].Split('/');
                        if (face_components.Length == 3)
                        {
                            pairs[1] = new KeyValuePair<double, double>(
                                Convert.ToDouble(face_components[0]),
                                Convert.ToDouble(face_components[2])
                                );
                        }
                        else
                        {
                            if (face_components.Length == 2)
                            {
                                pairs[1] = new KeyValuePair<double, double>(
                                    Convert.ToDouble(face_components[0]),
                                    Convert.ToDouble(face_components[1])
                                    );
                            }
                        }

                        face_components = components[i+1].Split('/');
                        if (face_components.Length == 3)
                        {
                            pairs[2] = new KeyValuePair<double, double>(
                                Convert.ToDouble(face_components[0]),
                                Convert.ToDouble(face_components[2])
                                );
                        }
                        else
                        {
                            if (face_components.Length == 2)
                            {
                                pairs[2] = new KeyValuePair<double, double>(
                                    Convert.ToDouble(face_components[0]),
                                    Convert.ToDouble(face_components[1])
                                    );
                            }
                        }

                        Faces.Add(pairs);

                    }
                }
            }

            Console.WriteLine("Chargement fini !");
        }

        public void ReverseNormals(int count)
        {
            for(int i = Normals.Count-1; i > (Normals.Count - 1) -count; i--)
            {
                Normals[i] = new Vector3D(-Normals[i].X, -Normals[i].Y, -Normals[i].Z);
            }
        }

        public void ToFile(string filename)
        {
            string mtlFilename = Path.ChangeExtension(filename, ".mtl");
            List<String> data = new List<string>();

            data.Add("# Glass project - (c)2019 Jean Varnoux");

            data.Add("mtllib " + Path.GetFileName(mtlFilename));
            data.Add("");
        

            foreach (Vector3D v in Vertices)
            {
                data.Add("v " + v.X.ToString().Replace(',', '.') + " " + v.Y.ToString().Replace(',', '.') + " " + v.Z.ToString().Replace(',', '.'));
            }
            data.Add("# " + Vertices.Count + " vertices");
            data.Add("");

            foreach (Vector3D vn in Normals)
            {
                data.Add("vn " + vn.X.ToString().Replace(',', '.') + " " + vn.Y.ToString().Replace(',', '.') + " " + vn.Z.ToString().Replace(',', '.'));
            }
            data.Add("# " + Normals.Count + " vertex normals");
            data.Add("");

            int countGroupes = 1;
            
            foreach (int[] numFaces in Groupes_face) {
                data.Add("g Face"+ countGroupes++);
                data.Add("usemtl wire_214229166");
                foreach (int numFace in numFaces)
                {
                    KeyValuePair<double, double>[] pairs = Faces[numFace];
                    if (pairs.Length == 3)
                    {
                        data.Add("f " + pairs[0].Key + "//" + pairs[0].Value + " " + pairs[1].Key + "//" + pairs[1].Value + " " + pairs[2].Key + "//" + pairs[2].Value);
                    }
                    else if (pairs.Length == 4)
                    {
                        data.Add("f " + pairs[0].Key + "//" + pairs[0].Value + " " + pairs[1].Key + "//" + pairs[1].Value + " " + pairs[2].Key + "//" + pairs[2].Value + " " + pairs[3].Key + "//" + pairs[3].Value);
                    }
                }

                data.Add("# " + numFaces.Length + " polygons");
                data.Add("");
            }

            int totaledge = 0;

            data.Add("g Edges");
            data.Add("usemtl wire_214229166");
            foreach (int[] numFaces in Groupes_edge)
            {
                
                foreach (int numFace in numFaces)
                {
                    KeyValuePair<double, double>[] pairs = Faces[numFace];
                    if (pairs.Length == 3)
                    {
                        data.Add("f " + pairs[0].Key + "//" + pairs[0].Value + " " + pairs[1].Key + "//" + pairs[1].Value + " " + pairs[2].Key + "//" + pairs[2].Value);
                    }
                    else if (pairs.Length == 4)
                    {
                        data.Add("f " + pairs[0].Key + "//" + pairs[0].Value + " " + pairs[1].Key + "//" + pairs[1].Value + " " + pairs[2].Key + "//" + pairs[2].Value + " " + pairs[3].Key + "//" + pairs[3].Value);
                    }
                    totaledge++;
                }
            }
            data.Add("# " + totaledge + " polygons");
            data.Add("");

            
            File.WriteAllLines(filename, data.ToArray());

            

            data.Clear();

            data.Add("newmtl wire_214229166");

            data.Add("Ns 32");

            data.Add("d 1");

            data.Add("Tr 0");

            data.Add("Tf 1 1 1");

            data.Add("illum 2");

            data.Add("Ka 0.8392 0.8980 0.6510");

            data.Add("Kd 0.8392 0.8980 0.6510");

            data.Add("Ks 0.3500 0.3500 0.3500");

            File.WriteAllLines(mtlFilename, data.ToArray());
        }
    }
}