﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Xml;

namespace glassApp
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        Model3D scene;
        Obj obj_base = new Obj();
        Obj obj_final;


        private void charger_obj_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".obj";
            dlg.Filter = "obj Files (*.obj)|*.obj";


            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;
                texte_obj.Text = filename;

                
            }
        }

        private void charger_xml_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".calumen";
            dlg.Filter = "calumen Files (*.calumen)|*.calumen|XML Files (*.xml)|*.xml";

            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;
                texte_xml.Text = filename;

            }
        }

        private void lancer_Click(object sender, RoutedEventArgs e)
        {
            if (handle_error())
                return;
            

            obj_base.LoadFromFile(texte_obj.Text);
            obj_final = new Obj();

            XmlDocument doc = new XmlDocument();
            doc.Load(texte_xml.Text);
            string xmlcontents = doc.InnerXml;
            XmlNodeList elemList = doc.GetElementsByTagName("GlazingElementContainer");
            Double thickness_cur = 0;
            for(int i=0; i < elemList.Count; i++)
            {
                for (int j = 0; j < elemList[i].Attributes.Count; j++) {
                    if (elemList[i].Attributes[j].Value == "GasCavity") {
                        thickness_cur = Convert.ToDouble(elemList[i]["Thickness"].InnerText);
                        if (thickness_cur > 0)
                        {
                            obj_base.Translate(thickness_cur);
                        }
                    }
                    else if (elemList[i].Attributes[j].Value == "SubGlazing")
                    {
                        XmlNodeList glasingElement = elemList[i]["GlazingElements"].GetElementsByTagName("GlazingElement");

                        for (int a = 0; a < glasingElement.Count; a++)
                        {
                            for (int b = 0; b < glasingElement[a].Attributes.Count; b++)
                            {
                                if (glasingElement[a].Attributes[b].Value == "Substrate")
                                {
                                    obj_final.AddObj(obj_base,false);
                                    thickness_cur = Convert.ToDouble(glasingElement[a]["Thickness"].InnerText);
                                    obj_base.Translate(thickness_cur);
                                    obj_final.AddObj(obj_base,true);
                                    obj_final.ReverseNormals(obj_base.Normals.Count);
                                    obj_final.Bridge(obj_base);
                                }
                            }
                        }
                    }
                }
            }

            // Declare scene objects.
            Model3DGroup myModel3DGroup = new Model3DGroup();
            ModelVisual3D myModelVisual3D = new ModelVisual3D();
            GeometryModel3D myGeometryModel = new GeometryModel3D();
            // Defines the camera used to view the 3D object. In order to view the 3D object,
            // the camera must be positioned and pointed such that the object is within view 
            // of the camera.
            PerspectiveCamera myPCamera = new PerspectiveCamera();

            // The geometry specifes the shape of the 3D plane. In this sample, a flat sheet 
            // is created.
            MeshGeometry3D myMeshGeometry3D = new MeshGeometry3D();

            
            Vector3DCollection myNormalCollection = new Vector3DCollection();
            for (int i = 0; i < obj_final.Vertices.Count; i++)
                myNormalCollection.Add(obj_final.VerticeNormal(i));
            myMeshGeometry3D.Normals = myNormalCollection;

            Point3DCollection myPositionCollection = new Point3DCollection();
            foreach (Vector3D vertice in obj_final.Vertices)
                myPositionCollection.Add(new Point3D(vertice.X, vertice.Y, vertice.Z));
            myMeshGeometry3D.Positions = myPositionCollection;


            Int32Collection myTriangleIndicesCollection = new Int32Collection();
            foreach (KeyValuePair<double, double>[] pairs in obj_final.Faces)
            {
                myTriangleIndicesCollection.Add((int)(pairs[0].Key-1));
                myTriangleIndicesCollection.Add((int)(pairs[1].Key-1));
                myTriangleIndicesCollection.Add((int)(pairs[2].Key-1));
            }
            myMeshGeometry3D.TriangleIndices = myTriangleIndicesCollection;

            myGeometryModel.Geometry = myMeshGeometry3D;

            // Create a horizontal linear gradient with four stops.   
            SolidColorBrush solidColorBrush = new SolidColorBrush(Colors.Red);

            // Define material and apply to the mesh geometries.
            DiffuseMaterial myMaterial = new DiffuseMaterial(solidColorBrush);
            myGeometryModel.Material = myMaterial;

            // Specify where in the 3D scene the camera is.
            myPCamera.Position = new Point3D(3, 1, 0);

            // Specify the direction that the camera is pointing.
            myPCamera.LookDirection = new Vector3D(-3, -1, 0);

            myPCamera.UpDirection = new Vector3D(0, 1, 0);



            // Define camera's horizontal field of view in degrees.
            myPCamera.FieldOfView = 60;

            // Asign the camera to the viewport
            myViewport3D.Camera = myPCamera;


            // Define the lights cast in the scene. Without light, the 3D object cannot 
            // be seen. Note: to illuminate an object from additional directions, create 
            // additional lights.
            DirectionalLight myDirectionalLight = new DirectionalLight();
            myDirectionalLight.Color = Colors.White;
            myDirectionalLight.Direction = new Vector3D(-0.61, -0.5, -0.61);

            myModel3DGroup.Children.Add(myDirectionalLight);

            myModel3DGroup.Children.Add(myGeometryModel);

            // Add the group of models to the ModelVisual3d.
            myModelVisual3D.Content = myModel3DGroup;

            myViewport3D.Children.Add(myModelVisual3D);

            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "Document"; // Default file name
            dlg.DefaultExt = ".obj"; // Default file extension
            dlg.Filter = "obj file (.obj)|*.obj"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                string filename = dlg.FileName;
                obj_final.ToFile(filename);




            }
        }   

        private bool handle_error()
        {
            bool hasError = false;
            
            if (String.IsNullOrEmpty(texte_obj.Text))
            {
                obj_error.Text = "Obj vide !";
                hasError = true;
            }
            else
            {
                obj_error.Text = "";
            }

            if (String.IsNullOrEmpty(texte_xml.Text))
            {
                xml_error.Text = "xml vide !";
                hasError = true;
            }
            else
            {
                xml_error.Text = "";
            }

            return hasError;
        }
    }
}
